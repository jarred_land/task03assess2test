﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            option03();
        }

        static void option03()
        {
            Console.WriteLine("Please enter 5 numbers between 1 and 5 and press <ENTER> after each number");

            var user = new int[5];
            int count = 0;

            for (count = 0; count < user.Length;)
            {
                string input = Console.ReadLine();
                int usernumber = 0;

                if (input == "")
                    continue;
                else if (int.TryParse(input, out usernumber))
                {
                    if (usernumber > 5 || usernumber < 1)
                    {
                        Console.WriteLine("The number you have entered is not between 1 and 5. Please try again");
                    }
                    else
                    {
                        user[count] = usernumber;
                        count++;
                    }
                }

                else
                {
                    Console.WriteLine("That's not a number. Please try again");
                }
            }

            var score = new List<int>();

            for (count = 0; count < user.Length; count++)
            {

                int userint = 0;
                bool value = int.TryParse(user[count].ToString(), out userint);

                Random r = new Random();

                int range = 5;
                int rInt = r.Next(1, range);

                if (rInt == userint)
                {
                    Console.WriteLine($"The random number was {rInt} and you guessed {userint}. You were correct!");
                    score.Add(rInt);
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine($"The random number was {rInt} and you guessed {userint}. You were incorrect.");
                    Console.ReadLine();
                }

            }

            Console.WriteLine($"Your score was {score.Count}");
            Console.WriteLine($"Your last scores were");
            Console.WriteLine("Would you like to play again? Press y for yes or m to retun to the main menu");
            var yesno = Console.ReadLine();

            if (yesno == "y")
            {
                Console.Clear();
                option03();
            }
            else
            {
                //add section to return to main menu
            }
        }
    }
}
