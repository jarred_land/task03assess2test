﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test3
{
    class Program
    {
        static void Main(string[] args)
        {
            option03();
        }

        static void option03()
        {
            Console.WriteLine("Please enter a number between 1 and 5");

            var user = new int[5];
            int count = 0;

            var counter = 6;
            var index = 0;

            while (index < counter)
            {
                string input = Console.ReadLine();
                int usernumber = 0;
                int.TryParse(input, out usernumber);

                if (input == "")
                {
                    if ((usernumber > 5) && (usernumber < 1))
                    {
                        Console.WriteLine("The number you have entered is not between 1 and 5. Please try again");
                    }
                    else
                    {
                        user[count] = usernumber;
                        count++;
                    }
                }

                else
                {
                    Console.WriteLine("That's not a number. Please try again");
                }

                var score = new List<int>();

                int userint = 0;
                bool value = int.TryParse(user[count].ToString(), out userint);

                Random r = new Random();

                int range = 5;
                int rInt = r.Next(1, range);

                if (rInt == userint)
                {
                    Console.WriteLine($"The random number was {rInt} and you guessed {userint}. You were correct!");
                    score.Add(rInt);
                }
                else
                {
                    Console.WriteLine($"The random number was {rInt} and you guessed {userint}. You were incorrect.");
                }

            }

           // Console.WriteLine($"Your score was {score.Count}");
            Console.WriteLine($"Your last scores were");
            Console.WriteLine("Would you like to play again? Press y for yes or m to retrn to the main menu");
            var yesno = Console.ReadLine();

            if (yesno == "y")
            {
                Console.Clear();
                option03();
            }
            else
            {
                //add section to return to main menu
            }

        }
    }
 }
